(use-modules ((gnu packages haskell-web) #:select (ghc-aeson))
             ((guix gexp) #:select (local-file))
             ((guix git-download) #:select (git-predicate))
             ((guix build-system haskell) #:select (haskell-build-system))
             ((guix licenses) #:select (gpl3))
             ((guix packages) #:select (package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "ghc-takkyuu")
    (version "0.1.0.0")
    (source
      (local-file %source-dir
                  #:recursive? #t
                  #:select? (git-predicate %source-dir)))
    (build-system haskell-build-system)
    (inputs (list ghc-aeson))
    (home-page "https://gitlab.liris.cnrs.fr/abrenon/takkyuu")
    (synopsis "A library to connect text analyzers bidirectionally")
    (description "")
    (license gpl3)))
