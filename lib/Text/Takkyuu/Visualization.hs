{-# LANGUAGE NamedFieldPuns #-}
module Text.Takkyuu.Visualization (
      dump
    , explore
    , render
  ) where

import Text.Takkyuu.Proto (Game(..))
import Text.Printf (printf)

data Tree a =
    Leaf Bool
  | Tree {
      unTree :: [(a, Tree a)]
    }
  deriving Show

explore :: Game a -> Tree a
explore (Over b) = Leaf b
explore (On {token, next, fallback}) =
  case explore fallback of
    Leaf _ -> Tree [(token, explore next)]
    Tree l -> Tree $ (token, explore next):l

dump :: Show a => Tree a -> String
dump = unlines . dumpAux
  where
    dumpAux (Leaf b) = [if b then "o" else "x"]
    dumpAux (Tree l) = l >>= f
    f (a, tree) = printf "%s — %s" (show a) <$> dumpAux tree

render :: Show a => Game a -> IO ()
render = putStr . dump . explore
