{-# LANGUAGE NamedFieldPuns #-}
module Text.Takkyuu.Proto (
      Game(..)
    , Label(..)
    , Labeled(..)
    , game
    , oneOf
    , fuzz
    , keep
    , ignore
    , dico
    , labelizer
    , fixTypo
    , rules
  ) where

import Control.Applicative (Alternative(..))
import Data.Map (Map, findWithDefault, insertWith)
import qualified Data.Map as Map (empty, lookup)
import Data.Set as Set (Set)
import qualified Data.Set as Set (empty, fromList, member)

data Game a =
    Over Bool
  | On {
          token :: a
        , next :: Game a
        , fallback :: Game a
      }
  deriving Show

instance Semigroup (Game a) where
  Over True <> g = g
  o@(Over False) <> _ = o
  o <> g = o {
        next = next o <> g
      , fallback = fallback o <> g
    }

instance Monoid (Game a) where
  mempty = Over True

instance Functor Game where
  fmap _  (Over b) = Over b
  fmap f o = o {
        token = f $ token o
      , next = f <$> next o
      , fallback = f <$> fallback o
    }

instance Applicative Game where
  pure token = On {
        token
      , next = mempty
      , fallback = empty
    }

  Over b <*> _ = Over b
  _ <*> Over b = Over b
  f <*> g = On {
        token = (token f) (token g)
      , next = (next f) <*> (next g)
      , fallback = (fallback f) <*> (fallback g)
    }

instance Alternative Game where
  empty = Over False
  Over False <|> g = g
  o@(Over True) <|> _ = o
  o <|> g = o {
      fallback = fallback o <|> g
    }

instance Monad Game where
  Over b >>= _ = Over b
  o >>= f = (f (token o) <> ((next o) >>= f)) <|> ((fallback o) >>= f)

game :: [a] -> Game a
game = foldr ((<>) . pure) mempty

oneOf :: [a] -> Game a
oneOf = foldr ((<|>) . pure) empty

fuzz :: (Eq a, Ord a) => Set [a] -> Game a -> Game a
fuzz classes = (>>= alternatives)
  where
    alternatives t = oneOf (findWithDefault [t] t equiv)
    equiv = foldl (\tmp0 l ->
        foldl (\tmp1 x -> 
            insertWith (++) x l tmp1
          ) tmp0 l
      ) Map.empty classes

data Label l =
    Unknown
  | Label l
  deriving (Eq, Ord, Show)

data Labeled a l = Labeled {
      item :: a
    , label :: Label l
  }
  deriving (Eq, Ord, Show)


keep :: (a -> Bool) -> Game a -> Game a
keep predicate = (>>= select)
  where
    select a
      | predicate a = pure a
      | otherwise = Over False

ignore :: (a -> Bool) -> Game a -> Game a
ignore p = keep (not . p)

dico :: (Eq k, Ord k) => (a -> k) -> (k -> Bool) -> Set [k] -> Game a -> Game [a]
dico projector isBlank vocabulary = continueFrom start
  where
    start = ([], vocabulary)
    continueFrom (stack@(_:_), candidates) (Over True)
      | not $ Set.member [] candidates = pure (reverse stack)
    continueFrom _ (Over b) = Over b
    continueFrom state@(stack, candidates) (On {token, next, fallback})
      | isBlank (projector token) =
            ((if null stack || Set.member [] candidates then empty else pure (reverse stack))
            <> continueFrom start next)
          <|> continueFrom state next
          <|> continueFrom state fallback
      | otherwise =
        let projection = projector token in
        let newCandidates = foldMap (startingWith projection) candidates in
        case (null newCandidates, Set.member [] newCandidates) of
          (True, _) -> continueFrom state fallback <|> continueFrom (token:stack, newCandidates) next
          (_, False) -> continueFrom (token:stack, newCandidates) next
                      <|> continueFrom state fallback
          _ -> (pure (reverse $ token:stack) <> continueFrom start next)
              <|> continueFrom (token:stack, newCandidates) next
              <|> continueFrom state fallback
    startingWith e (x:xs) = Set.fromList $ if e == x then [xs] else []
    startingWith _ [] = Set.empty

rules :: (Eq k, Ord k) => (a -> k) -> Set [k] -> Game a -> Game [a]
rules projector vocabulary = continueFrom start
  where
    start = ([], vocabulary)
    continueFrom _ (Over b) = Over b
    continueFrom state@(stack, candidates) (On {token, next, fallback}) =
      let projection = projector token in
      let newCandidates = foldMap (startingWith projection) candidates in
      case (null newCandidates, Set.member [] newCandidates) of
        (True, _) -> continueFrom state fallback
        (_, False) -> continueFrom (token:stack, newCandidates) next
                    <|> continueFrom state fallback
        _ -> (pure (reverse $ token:stack) <> continueFrom start next)
            <|> continueFrom (token:stack, newCandidates) next
            <|> continueFrom state fallback
    startingWith e (x:xs) = Set.fromList $ if e == x then [xs] else []
    startingWith _ [] = Set.empty

labelizer :: Ord k => (a -> k) -> (Map k [b]) -> Game a -> Game (Labeled a b)
labelizer projector labels = (>>= alternatives)
  where
    alternatives t =
      let possibleLabels = Map.lookup (projector t) labels in
      oneOf $  maybe [Labeled t Unknown] (Labeled t . Label <$>) possibleLabels

fixTypo :: Game Char -> Game Char
fixTypo = fuzz $ Set.fromList [
      "bh"
    , "il1"
  ]
