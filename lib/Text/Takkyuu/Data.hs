{-# LANGUAGE NamedFieldPuns #-}
module Text.Takkyuu.Data (
  ) where

data Move a =
    Here a
  | Back

data Reply a =
    Ok
  | No

data Round a =
    Over Bool
  | On {
        move :: Move a
      , next :: Reply a -> Round a
    }

fromList :: [a] -> Round a
fromList [] = Over True
fromList (a:as) = On {
      move = Here a
    , next
  }
  where
    next Ok = fromList as
    next _ = Over False
