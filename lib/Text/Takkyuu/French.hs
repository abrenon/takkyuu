{-# LANGUAGE NamedFieldPuns #-}
module Text.Takkyuu.French (
      POS(..)
    , Gender(..)
    , MorphoLabel(..)
    , french
    , tag
    , syntax
  ) where

import Data.Char (isSpace, toLower)
import qualified Data.Map as Map (fromList)
import qualified Data.Set as Set (fromList)
import Text.Takkyuu.Proto (Game, Labeled(..), Label(..), dico, labelizer, rules)

data POS =
    Determiner
  | Noun
  | Particip
  deriving (Eq, Ord, Show)

data Gender =
    Masculine
  | Feminine
  deriving (Eq, Ord, Show)

data Number =
    Singular
  | Plural
  deriving (Eq, Ord, Show)

data MorphoLabel = MorphoLabel {
      pos :: POS
    , gender :: Gender
    , number :: Number
  }
  deriving (Eq, Ord, Show)

type LabeledWord = Labeled String MorphoLabel

french :: Game Char -> Game String
french = dico id isSpace $ Set.fromList ["le", "la", "chat", "lait", "marché"]

tag :: Game String -> Game LabeledWord
tag = labelizer (fmap toLower) $ Map.fromList [
      ("le",
        [MorphoLabel {pos = Determiner, gender = Masculine, number = Singular}])
    , ("la",
        [MorphoLabel {pos = Determiner, gender = Feminine, number = Singular}])
    , ("les", [
            MorphoLabel {pos = Determiner, gender = Masculine, number = Plural}
          , MorphoLabel {pos = Determiner, gender = Feminine, number = Plural}
        ])
    , ("chat", [MorphoLabel {pos = Noun, gender = Masculine, number = Singular}])
    , ("lait", [MorphoLabel {pos = Noun, gender = Masculine, number = Singular}])
    , ("marché", [
            MorphoLabel {pos = Noun, gender = Masculine, number = Singular}
          , MorphoLabel {pos = Particip, gender = Masculine, number = Singular}
        ])
  ]

syntax :: Game LabeledWord -> Game [LabeledWord]
syntax = rules label . Set.fromList $ agreeSn <$> combinations
  where
    combinations = [(g, n) | g <- [Masculine, Feminine], n <- [Singular, Plural]]
    agreeSn (gender, number) = [
          Label $ MorphoLabel {pos = Determiner, gender, number}
        , Label $ MorphoLabel {pos = Noun, gender, number}
      ]
