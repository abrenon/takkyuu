module Text.Takkyuu.IO (
      fromHandle
    , toHandle
  ) where

import Text.Takkyuu.Data (Round(..))

toHandle :: Handle -> Round a -> IO ()

fromHandle :: Handle -> IO (Round a)
